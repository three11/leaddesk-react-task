import React from 'react';
import Listing from './components/listing/Index';
import AddProductForm from './components/form/AddProductForm';
import Header from './components/Header';
import Title from './components/Title';
import logo from './logo.svg';

import './App.css';

function App() {
  return (
    <div className="App">
      <div className="shopping-list-wrapper">
        <div className="shell">
          <Header logo={logo} text="Shopping List" />

          <Listing />

          <Title text="Add new products" />

          <AddProductForm />
        </div>
      </div>
    </div>
  );
}

export default App;
