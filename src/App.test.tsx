import { act } from 'react-dom/test-utils';
import { fireEvent, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './config/store';
import App from './App';

jest.mock('./helpers', () => ({
  ...jest.requireActual('./helpers'),
  generateRandomProducts: jest.fn(() => [
    { id: 0, name: 'Mocked product name', amount: 10 },
    { id: 1, name: 'Mocked product name', amount: 10 },
    { id: 2, name: 'Mocked product name', amount: 10 },
    { id: 3, name: 'Mocked product name', amount: 10 },
    { id: 4, name: 'Mocked product name', amount: 10 },
    { id: 5, name: 'Mocked product name', amount: 10 },
    { id: 6, name: 'Mocked product name', amount: 10 },
    { id: 7, name: 'Mocked product name', amount: 10 },
    { id: 8, name: 'Mocked product name', amount: 10 },
    { id: 9, name: 'Mocked product name', amount: 10 },
  ]),
}));

test('render add product form', async () => {
  const { queryByText, asFragment, container } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  const productElement = queryByText('Add new products');

  expect(asFragment()).toMatchSnapshot();

  expect(productElement).toBeInTheDocument();

  let products = Array.from(container.querySelectorAll('tbody tr'));

  expect(products.length).toBe(10);

  const firstButton = products[0].querySelector('button');

  fireEvent.click(firstButton!);

  products = Array.from(container.querySelectorAll('tbody tr'));

  expect(products.length).toBe(9);

  const addButton = container.querySelector('form button');
  const addField = container.querySelector('form input[type="text"]');

  fireEvent.input(addField!, {
    target: { value: 'Another mocked product name' },
  });

  await act(async () => {
    await fireEvent.click(addButton!);
  });

  products = Array.from(container.querySelectorAll('tbody tr'));

  expect(products.length).toBe(10);
});
