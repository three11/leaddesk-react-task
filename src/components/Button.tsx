import React from 'react';

interface IProps {
  label: string;
  onClick: () => void;
}

const Button: React.FC<IProps> = ({ label, onClick }) => {
  return (
    <button onClick={onClick} className="button">
      {label}
    </button>
  );
};

export default Button;
