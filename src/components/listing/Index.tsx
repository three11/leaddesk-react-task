import { useMemo } from 'react';
import useAppSelector from '../../features/shopping-list/hooks/useAppSelector';
import selectProducts from '../../features/shopping-list/selectors/selectProducts';

import Item from './Item';
import './Styles.css';

const Index = () => {
  const products = useAppSelector(selectProducts);
  const columns = useMemo(() => ['Name', 'Amount', 'Action'], []);

  return (
    <div className="listing">
      <table>
        <thead>
          <tr>
            {columns.map((col, index) => (
              <th className={col.toLowerCase()} key={index}>
                {col}
              </th>
            ))}
          </tr>
        </thead>

        <tbody>
          {products.map(row => (
            <Item key={row.id} {...row} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Index;
