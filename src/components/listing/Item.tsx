import React, { FC } from 'react';
import { IProduct } from '../../features/shopping-list/types/product';
import {
  removeProduct,
  updateProduct,
} from '../../features/shopping-list/shoppingListSlice';
import useAppDispatch from '../../features/shopping-list/hooks/useAppDispatch';
import Button from '../Button';

const Item: FC<IProduct> = ({ id, name, amount }: IProduct) => {
  const dispatch = useAppDispatch();

  return (
    <tr>
      <td className="name">
        <input
          type="text"
          value={name}
          onChange={event => {
            // Prevents empty value
            const name = event.target.value;
            if (!name) {
              return;
            }

            dispatch(
              updateProduct({
                id: id,
                name: name,
                amount: amount,
              })
            );
          }}
        />
      </td>

      <td className="amount">
        <input
          type="number"
          required
          min="1"
          value={amount}
          onChange={event => {
            // Prevents empty quantity or zero input
            const quantity = Number(event.target.value);
            if (quantity < 1) {
              return;
            }

            dispatch(
              updateProduct({
                id: id,
                name: name,
                amount: quantity,
              })
            );
          }}
        />
      </td>

      <td className="action">
        <Button
          label="remove"
          onClick={() => dispatch(removeProduct(id))}
        ></Button>
      </td>
    </tr>
  );
};

export default React.memo(Item);
