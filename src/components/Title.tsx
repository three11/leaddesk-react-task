const Title = ({ text }: { text: string }) => {
  return <h3>{text}</h3>;
};

export default Title;
