import { useCallback } from 'react';
import useAppDispatch from '../../features/shopping-list/hooks/useAppDispatch';
import { setProduct } from '../../features/shopping-list/shoppingListSlice';
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from 'formik';

import {
  IFormFields,
  IFormErrorValidation,
} from '../../features/shopping-list/types/form';

const AddProductForm = () => {
  const dispatch = useAppDispatch();
  const initialValues: IFormFields = {
    name: '',
    amount: 1,
  };

  const onSubmit = useCallback(
    (values: IFormFields, formikHelpers: FormikHelpers<IFormFields>) => {
      dispatch(
        setProduct({
          name: values.name,
          amount: values.amount,
        })
      );
      formikHelpers.setSubmitting(false);
      formikHelpers.resetForm();
    },
    [dispatch]
  );

  const validate = (values: IFormFields) => {
    const errors: Partial<IFormErrorValidation> = {};

    if (!values.name) {
      errors.name = 'Product name is required';
    }

    if (isNaN(values.amount)) {
      errors.amount = 'Product amount must be a number';
    }

    if (!(values.amount && Number(values.amount) > 0)) {
      errors.amount = 'Product amount is required';
    }

    return errors;
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validate}
    >
      {({ isSubmitting }) => (
        <Form>
          <div className="row">
            <div className="column column-3of5">
              <Field type="text" name="name" placeholder="name" />
              <ErrorMessage
                name="name"
                component="div"
                className="error-message"
              />
            </div>
            <div className="column column-1of5">
              <Field type="number" name="amount" placeholder="amount" />
              <ErrorMessage
                name="amount"
                component="div"
                className="error-message"
              />
            </div>
            <div className="column column-1of5">
              <button
                type="submit"
                disabled={isSubmitting}
                className="button button-submit"
              >
                add
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default AddProductForm;
