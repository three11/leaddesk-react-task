import React from 'react';
interface IHeader {
  logo: string;
  text: string;
}

const Header = ({ logo, text }: IHeader) => {
  return (
    <header className="shopping-list-header">
      <div className="row">
        <div className="column column-1of5">
          <img src={logo} className="app-logo" alt="logo" />
        </div>
        <div className="column column-4of5">
          <h1>{text}</h1>
        </div>
      </div>
    </header>
  );
};

export default React.memo(Header);
