import { faker } from '@faker-js/faker';

export const generateRandomNumber = (minimum: number, maximum: number) => {
  return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
};

export const generateRandomProducts = (count: number) => {
  return Array(count)
    .fill('')
    .map((_, index: number) => ({
      id: index,
      name: faker.commerce.productName(),
      amount: generateRandomNumber(1, 20),
    }));
};
