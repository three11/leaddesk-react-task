import { useDispatch } from 'react-redux';
import type { AppDispatch } from '../../../config/store';

const useAppDispatch = () => useDispatch<AppDispatch>();

export default useAppDispatch;
