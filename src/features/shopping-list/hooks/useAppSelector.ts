import { TypedUseSelectorHook, useSelector } from 'react-redux';
import type { RootState } from '../../../config/store';

const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export default useAppSelector;
