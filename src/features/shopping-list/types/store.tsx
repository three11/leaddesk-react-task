import { IProduct } from './product';

export interface IShoppingListState {
  products: IProduct[];
}
