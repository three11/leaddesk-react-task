export interface IFormFields {
  name: string;
  amount: number;
}

export interface IFormErrorValidation {
  name: string;
  amount: string;
}
