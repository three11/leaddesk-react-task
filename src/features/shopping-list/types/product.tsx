export interface IProduct {
  id: number;
  name: string;
  amount: number;
}

export interface INewProduct {
  name: string;
  amount: number;
}
