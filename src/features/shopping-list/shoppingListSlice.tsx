import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IProduct, INewProduct } from './types/product';
import { IShoppingListState } from './types/store';
import { generateRandomProducts, generateRandomNumber } from '../../helpers';

const initialState: IShoppingListState = {
  products: generateRandomProducts(generateRandomNumber(5, 15)),
};

export const shoppingList = createSlice({
  name: 'shoppingList',
  initialState,
  reducers: {
    setProduct: (state, action: PayloadAction<INewProduct>) => {
      let nextID = 1;

      if (state.products.length) {
        nextID = state.products.slice(-1)[0]?.id;
        nextID++;
      }

      state.products.push({
        id: nextID,
        name: action.payload.name,
        amount: action.payload.amount,
      });
    },
    removeProduct: (state, action: PayloadAction<number>) => {
      const index = state.products.findIndex(
        product => product.id === action.payload
      );
      if (index !== -1) {
        state.products.splice(index, 1);
      }
    },
    updateProduct: (state, action: PayloadAction<IProduct>) => {
      const index = state.products.findIndex(
        product => product.id === action.payload.id
      );

      if (index !== -1) {
        state.products[index] = action.payload;
      }
    },
  },
});
export const { setProduct, removeProduct, updateProduct } =
  shoppingList.actions;

export default shoppingList.reducer;
