import { RootState } from '../../../config/store';

const selectProducts = (state: RootState) => state.shoppingList.products;

export default selectProducts;
