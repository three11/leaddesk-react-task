# LeadDesk React Task

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) TS template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Description

This is a simple application that allows the customer to store information about products in a list. The list can be easely edited.
The form allows easy and quick addition of products. The single product contains information about its name and quantity.
The names and quantities of already added products can be edited from the table through form inputs.

The application can be extended by sending the information to rest API service which can store the product list for further use.

Currently, I have applied random product generation on the initial app launch to recreate actual output from remote service

## Used libraries

### Redux Toolkit

The project uses the Redux Toolkit because it provides a simpler and more efficient way to manage the global state of our application. By using the Redux Toolkit, we can take advantage of pre-built functions and utilities that help to reduce the amount of boilerplate code needed to set up a Redux store and define reducers and actions.

### Faker

Faker.js provides a streamlined and effective method to create fake data for testing and development purposes.

### Formik

Formik is a library that helps simplify the management of forms in React. With Formik, you can easily manage the state of your forms, handle form validation, and even handle form submission. Formik allows you to define validation rules for your form fields and will handle the validation of those fields automatically.

Reduces boilerplate code: Formik helps reduce the amount of boilerplate code that you need to write for handling forms in React. With Formik, you can easily define your form fields and validation rules using a simple declarative syntax.
